<?php 


 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>wordpress test</title>
    <?php wp_enqueue_script("jquery"); ?>
 	<?php wp_head(); ?>
<script src="/wp-content/themes/test/assets/scripts/app.js"></script>
 </head>
 <body  <?php body_class(); ?> >
 <div class="wrapper">
	
 	<?php if (is_front_page()): ?>
    <div class="logo"><a href="#"><span class="icon icon-logo"></span></a></div>
        <nav class="navheader">
		  <?php 
        	if (has_nav_menu('primary')) :
        	wp_nav_menu(['theme_location' => 'primary', 'container' => true, 'menu_class' => 'nav navbar-nav']);
        	endif;
    	   ?>
        </nav>
 	<?php else: ?>
        <nav class="navheader-blog">
			<?php 
        	if (has_nav_menu('blog')) :
        	wp_nav_menu(['theme_location' => 'blog', 'container' => 'div', 'menu_class' => 'nav navbar-nav']);
        	endif;
    		?>
            <?php if ( have_posts() ) : ?>
                <div class="posts_menu">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="post_menu">
                            <a href="<?php the_permalink();?>" ><h2 class="posttitle_menu"><?php the_title();?> </h2></a>                            
                        </div>
                    <?php endwhile; ?>
                </div>
    <?php endif; ?>
        </nav>
 	<?php endif ?>
 	
 

 	

 
