<?php


get_header();
//page_on_front REMEMBER 
?>


    <?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>    
    <div class="intro" style='background: 
    linear-gradient(
      rgba(0,255,217,0.8), 
      rgba(0,255,217,0.8)
    ),
    url(<?php $back_image=get_field("background_intro"); echo $back_image["url"];?>);
    background-repeat: no-repeat;
    width:100%;
    height: 100vh;
    background-size:cover;'> 
    
    <div class="textgen"><div class="text1"><?php  the_field("subtitle"); ?></div>
    <div class="special"><?php the_field("title"); ?> </div>

    </div>
    </div>
    <div class="portfolio"> 
    <h2 class="titles">PORTAFOLIO</h2>
        <?php if ( have_rows("portfolio_images") ) : ?>
            <div class="images">
           <?php while ( have_rows("portfolio_images") ) : the_row(); ?>
            
                <?php $image=get_sub_field("image");?>
                   <div class="image"> <img src=" <?php echo $image["url"];?> " alt=""></div>
        
            <?php endwhile; ?>
            </div>
        <?php endif; ?> 
   
    </div>
    <div class="servicios"><h2 class="titles">SERVICIOS</h2> 
        
            <div class="services">
                   <div class="image"> <span class="icon icon-group"> </span>
                        <div class="servicetext"> Diseño Web/Móvil </div>
                        
                   </div>
                   <div class="image"> <span class="icon icon-brush"> </span>
                        <div class="servicetext"> Diseño UX </div>
                        
                   </div>
                   <div class="image"> <span class="icon icon-mobile"> </span>
                        <div class="servicetext"> Branding </div>
                        
                   </div>
            </div>

    </div>
    <div class="contactame" style='background: linear-gradient(rgba(0,255,217,0.8),rgba(0,255,217,0.8)),url(" <?php $back_image=get_field('background_contact'); echo $back_image['url']; ?> ") ;background-size:100%;background-position: center;'>
    <h2 class="titles">CONTACTAME</h2> 

<div class="forms"> <?php echo do_shortcode( '[contact-form-7 id="1234" title="Contact form 1"]' ); ?> </div>

    </div>
        
    <!-- ending loop-->
    <?php endwhile; ?>
    <?php endif; ?>
</div>

<?php

get_footer();