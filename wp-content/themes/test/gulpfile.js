var gulp = require("gulp");
var prefixer = require("gulp-autoprefixer");
var	sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var minify = require("gulp-clean-css");
var rename = require("gulp-rename");
var browserSync      = require('browser-sync').create();


gulp.task("styles",function(){
return gulp.src("assets/styles/*.scss")
	.pipe(sourcemaps.init())
	.pipe(sass(
			{
			sourceComments: 'map',
  			sourceMap: 'sass',
  			outputStyle: 'nested',
  			lineNumbers: true
  			}
  			))
	.pipe(prefixer({browsers: ['last 3 versions', '> 1%']}))
	.pipe(sourcemaps.write("/"))
	.pipe(gulp.dest("dist/styles"))
	.pipe(minify())
	.pipe(rename("main.min.css"))
	.pipe(gulp.dest("dist/styles"))


});

gulp.task("watch",function(){
browserSync.init({
	files:["assets/styles/*.scss"],
	proxy:"wordpresstest.dev"

})
gulp.watch("assets/styles/*.scss",["styles"]);


});

gulp.task("default",["styles","watch"]);