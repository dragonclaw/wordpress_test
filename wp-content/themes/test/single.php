<?php
    
get_header();

?>

<!-- Start the Loop. -->
<?php if ( have_posts() ) : ?>
    
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="fixed" style="background: linear-gradient(
        rgba(0,255,217,0.8), 
        rgba(0,255,217,0.8)
    ), url('<?php the_post_thumbnail_url(); ?>') no-repeat fixed center;">
    <div class="title_post">
        <h1><?php the_title();?></h1>
    </div>
    
    <div class="metadata_fixed">
                    <span class="date"><?php the_time('F j, Y');?></span>
                    <span class="author">By <?php the_author();?></span></br>
                </div>
</div>
    <div class="posts">
            <div class="post">
                <h1 class="posttitle_blog"><?php the_title();?> </h2>
                <div class="pcontent"><?php the_content();?></div>
                <div class="metadata">
                    <span class="date"><?php the_time('F j, Y');?></span>
                    <span class="author">By <?php the_author();?></span></br>
                </div>
            </div>
                    <div class="share"><span class="textshare">Share it with your friends!</span>
                    <ul class="social">
                        <li class="fa fa-twitter-square fa-2x" aria-hidden="true"><a href="www.twitter.com.ve"></a></li>
                        <li class="fa fa-facebook-square fa-2x" aria-hidden="true"></li>
                        <li class="fa fa-google-plus fa-2x" aria-hidden="true"></li>
                        <li class="fa fa-instagram fa-2x" aria-hidden="true"></li>
                    </ul>
                    </div>  
    <?php endwhile; ?>
    </div>
<?php endif; ?>