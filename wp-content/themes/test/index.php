<?php


get_header();

?>
<div class="wrapper"> 
<h4> <?php  echo the_field("nickname");   ?> </h4>
<?php
    if ( have_posts() ) :
        // Start the Loop.
        while ( have_posts() ) : the_post();
        ?><h2><a href="<?php the_permalink();  ?> "><?php	the_title();?> </a></h2>
		<span><?php	
		the_author();
		?></span>
         <?php

        the_content();
        endwhile;
    endif;
?>

</div>



<?php

get_footer();

