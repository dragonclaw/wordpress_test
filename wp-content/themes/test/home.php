<?php
    
get_header();

$back_image = get_field('blog_background', 'option');

?>

<div class="fixed" style="background: linear-gradient(
        rgba(0,255,217,0.8), 
        rgba(0,255,217,0.8)
    ), url(<?php echo $back_image['url']; ?>);">
    <div class="title">
        <h1>TITULO BLOG</h1>
    </div>
    <div class="subtitle">
        <h2>SUBTITULO BLOG</h2>
    </div>
</div>

<!-- Start the Loop. -->
<?php if ( have_posts() ) : ?>
	<div class="posts">
    <?php while ( have_posts() ) : the_post(); ?>
        
            <div class="post">
                <a href="<?php the_permalink();?>" ><h2 class="posttitle"><?php the_title();?> </h2></a>
                <div class="pcontent"><?php the_excerpt();?></div>
                <div class="metadata">
    				<span class="date"><?php the_time('F j, Y');?></span>
    				<span class="author">By <?php the_author();?></span>
				</div>
            </div>
    <?php endwhile; ?>
    </div>
<?php endif; ?>